#include "modbusRead.h"

ModbusRead::ModbusRead()
{
    _port = new ConfigurableSoftwareSerial(UART_RX_PIN, UART_TX_PIN, false, UART_DATA_BUFF);
}

ModbusRead::~ModbusRead()
{
    delete _port;
}

void ModbusRead::begin()
{  
    _port->begin(UART_BAUD,UART_STOP_BITS,UART_PARITY,UART_DATA_BITS);

    delay(1000);

    if (UART_BAUD > 19200) {
        _t15 = 750;
        _t35 = 1750;
    } else {
        _t15 = 15000000/UART_BAUD; // 1T * 1.5 = T1.5
        _t35 = 35000000/UART_BAUD; // 1T * 3.5 = T3.5
    }
}

bool ModbusRead::setSlaveId(byte slaveId)
{
    _slaveId = slaveId;
    return true;
}

byte ModbusRead::getSlaveId()
{
    return _slaveId;
}

void ModbusRead::task()
{
    _len = 0;
    while (_port->available() > _len)
    {
        _len = _port->available();
        delayMicroseconds(_t15);
    }

    if (_len == 0)
        return;

    byte i;
    if (_len != _DATA_SIZE)
    {
        for (int x = 0; x < _len; x++) _port->read();
        return;
    }

    _frame = (byte *)malloc(_len);
    for (i = 0; i < _len; i++)
    {
        _frame[i] = _port->read();
        data[i] = _frame[i];
    }
    if (receive(_frame) && _len == _DATA_SIZE) dataCorrect = true;
     _len = 0;
    free(_frame);
}

bool ModbusRead::receive(byte *frame)
{
    //first byte of frame = address
    byte address = frame[0];
    //Last two bytes = crc
    uint16_t crc = ((frame[_len - 2] << 8) | frame[_len - 1]);
    //Slave Check
    if (address != 0xFF && address != this->getSlaveId())
    {
        return false;
    }
    //CRC Check
    if (crc != this->calcCrc(_frame[0], _frame + 1, _len - 3))
    {
        return false;
    }
    return true;
}

word ModbusRead::calcCrc(byte address, byte *pduFrame, byte pduLen)
{
    byte CRCHi = 0xFF, CRCLo = 0x0FF, Index;

    Index = CRCHi ^ address;
    CRCHi = CRCLo ^ _auchCRCHi[Index];
    CRCLo = _auchCRCLo[Index];

    while (pduLen--)
    {
        Index = CRCHi ^ *pduFrame++;
        CRCHi = CRCLo ^ _auchCRCHi[Index];
        CRCLo = _auchCRCLo[Index];
    }

    return (CRCHi << 8) | CRCLo;
}

void ModbusRead::clearData()
{
    for (int x = 0; x < _DATA_SIZE; x++)
        data[x] = 0;
}