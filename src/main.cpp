//////
// this version if machine drying loop wifi reset esp not active.
// add EEPROM flag machine drying.
//////
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <MQTTClient.h>
#include <ArduinoJson.h>
#include <EEPROM.h>
#include "main.h"
#include "sm_timetask.h"

// ModbusRead mb;
WiFiClient wfClient;
MQTTClient mqttClient;
SM_TIMETASK timetask;

IPAddress ip;
IPAddress gateway(192, 168, 2, 1);
IPAddress subnet(255, 255, 255, 128);

void setup()
{
    pinMode(_PIN_LED_STS, OUTPUT);
    pinMode(_PIN_IO4, OUTPUT);
    pinMode(_PIN_IO5, OUTPUT);
    pinMode(_PIN_LED_STS, OUTPUT);
    digitalWrite(_PIN_LED_STS, HIGH);

    EEPROM.begin(10);
    Serial1.begin(9600);
    Serial.begin(9600, SERIAL_8O1);
    Serial1.println();
    Serial1.println();
    Serial1.println("Otteri dry machine program v203 start...");

    modbus_setSlaveId(1);
    getMachineID();

    //init EEPROM address flag machine drying
    if (EEPROM.read(_ADDRESS_MACHINE_DRYING_A) > 1)
    {
        EEPROM.write(_ADDRESS_MACHINE_DRYING_A, 0);
        EEPROM.commit();
    }
    if (EEPROM.read(_ADDRESS_MACHINE_DRYING_B) > 1)
    {
        EEPROM.write(_ADDRESS_MACHINE_DRYING_B, 0);
        EEPROM.commit();
    }

    ////check esp reset in drying.
    if (EEPROM.read(_ADDRESS_MACHINE_DRYING_A) == 1)
    {
        MachineDryLastStatus_A = 1;
    }
    if (EEPROM.read(_ADDRESS_MACHINE_DRYING_B) == 1)
    {
        MachineDryLastStatus_B = 1;
    }

//WiFi Configuration
#ifdef _WIFI_LOCAL
    ip = IPAddress(192, 168, 2, (MachineID1));
    Serial1.println("fix ip.");
    WiFi.config(ip, gateway, subnet);
    WiFi.mode(WIFI_STA);
    WiFi.begin(WiFissid, WiFipass);
#else
    Serial1.println("don't fix ip.");
    WiFi.mode(WIFI_STA);
    WiFi.begin(WiFissid, WiFipass);
#endif
    //MQTT initial
    mqttClient.begin(MQTT_Server, MQTT_Port, wfClient);
    mqttClient.onMessage(messageReceived);

    ConnectWiFi();
    delay(1000);
    timecheckMachineDry_A = millis();
    timecheckMachineDry_B = millis();
}

void loop()
{
    timetask.Start();
    checkResetESP();
    // //read dara from modbus.
    // modbus_task();

    if (modbus_dataCorrect)
    {
        getTotalMoney();
        getMoneyReceive();
        checkDryEven_A();
        checkDryEven_B();
        getMachineStatus();
        getErrorCode();
        checkSettingMode();

        if (millis() - setTime > 2000)
        {
            if (checkwifiDisCon == false)
            {
                Serial1.print(F("Count WifiDisCon :"));
                Serial1.println(wifidisConCnt);
            }

            getModeSelect();
            loopStatusWork();
            setTime = millis();
        }

        modbus_dataCorrect = false;
        modbus_clearData();
    }

    loopSendStatus(5000);
    if ((millis() - timeCheckTransBuf > _LOOP_TIME_CHK_TRANBUF) || (millis() < timeCheckTransBuf))
    {
        LoopCheckRemainBuff();
        timeCheckTransBuf = millis();
    }

    if (Machine_A_Normal && MachineDryLastStatus_A)
    {
        if (millis() - timecheckMachineDry_A > 10000)
        {
            MachineDryLastStatus_A = 0;
        }
    }
    if (Machine_A_Normal && MachineDryLastStatus_B)
    {
        if (millis() - timecheckMachineDry_B > 10000)
        {
            MachineDryLastStatus_B = 0;
        }
    }
}

void ConnectWiFi()
{
    String _buff1 = F("MQTT Server = ");
    String _buff2 = F("MQTT User Name = ");
    String _buff3 = F("MQTT Client Name = ");
    String _buff4 = F("MQTT Port = ");
    if (WiFi.status() != WL_CONNECTED)
    {
        Serial1.print(F("Connecting to "));
        Serial1.print(WiFissid);
        Serial1.println(F("..."));
        flagWifiConnected = false;

        if (WiFi.waitForConnectResult() != WL_CONNECTED)
        {
            Serial1.print(F("can not connect to "));
            Serial1.println(WiFissid);
            // ESP.restart();
            return;
        }

        // Port defaults to 8266
        ArduinoOTA.setPort(8266);

        // Hostname defaults to esp8266-[ChipID]
        Serial1.print(F("Host name ota : "));
        Serial1.println(MQTT_ClientName);
        ArduinoOTA.setHostname(MQTT_ClientName.c_str());

        // No authentication by default
        ArduinoOTA.setPassword((const char *)"#cirbox2012");

        ArduinoOTA.onStart([]() {
            Serial1.println("Start");
            pinMode(_DEBUG_PIN_RX0, INPUT);
            pinMode(_DEBUG_PIN_TX0, OUTPUT);
        });
        ArduinoOTA.onEnd([]() {
            Serial1.println("\nEnd");
        });
        ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
            Serial1.printf("Progress: %u%%\r", (progress / (total / 100)));
        });
        ArduinoOTA.onError([](ota_error_t error) {
            Serial1.printf("Error[%u]: ", error);
            if (error == OTA_AUTH_ERROR)
                Serial1.println("Auth Failed");
            else if (error == OTA_BEGIN_ERROR)
                Serial1.println("Begin Failed");
            else if (error == OTA_CONNECT_ERROR)
                Serial1.println("Connect Failed");
            else if (error == OTA_RECEIVE_ERROR)
                Serial1.println("Receive Failed");
            else if (error == OTA_END_ERROR)
                Serial1.println("End Failed");
        });
        ArduinoOTA.begin();

        Serial1.println(F("WiFi connected"));
        Serial1.print(F("My ip address "));
        Serial1.println(WiFi.localIP());
    }
    if (WiFi.status() == WL_CONNECTED)
    {
        flagWifiConnected = true;
        ArduinoOTA.handle();

        if (!mqttClient.connected())
        {
            mqttClient.connect(MQTT_ClientName.c_str(), MQTT_UserName, MQTT_Password);
            if (mqttClient.connected())
            {
                Serial1.println(F("MQTT Broker Connecting."));
                Serial1.println(_buff1 + String(MQTT_Server));
                Serial1.println(_buff2 + String(MQTT_UserName));
                Serial1.println(_buff3 + String(MQTT_ClientName));
                Serial1.println(_buff4 + String(MQTT_Port));
                Serial1.println(F("MQTT Broker Connected."));
                mqttClient.subscribe(MQTT_SubTopicReturn);
                mqttClient.subscribe(MQTT_SubTopicCHID);
                // debugSerial.print(F("MQTT subscribe topic : "));
                // debugSerial.println(MQTT_SubTopicReturn);

                mqttClient.publish(MQTT_PubTopicDebug, "" + String(MachineID1) + "," + String(MachineID2) + " Dry machine sofware version 203 start");
                mqttClient.publish(MQTT_PubTopicDebug, "My ip :" + WiFi.localIP().toString());
                //initial message id
                mqttClient.publish("report", "{\"table\":\"2\",\"mid\":-1,\"api\":{\"1\":\"" + String(MachineID1) + "\",\"2\":\"-\",\"3\":\"-\",\"4\":\"-\",\"5\":\"-\"}}");
                mqttClient.publish("report", "{\"table\":\"2\",\"mid\":-1,\"api\":{\"1\":\"" + String(MachineID2) + "\",\"2\":\"-\",\"3\":\"-\",\"4\":\"-\",\"5\":\"-\"}}");
            }
        }

        if (mqttClient.connected())
        {
            //digitalWrite(ClientStatusPin, LOW);
            flagMQTTConnected = true;
        }
        else
        {
            Serial1.println(F("MQTT disconnect."));
            flagMQTTConnected = false;
            delay(300);
        }
    }
}

void appTask1ms()
{
    //Serial.println("#1ms");
    readMBloop++;
    if (readMBloop == 5)
    {
        modbus_task();
        readMBloop = 0;
    }
}

void appTask10ms()
{
    //Serial1.println("#10ms");
    //add new
    if (WiFi.status() == WL_CONNECTED && mqttClient.connected())
    {
        // Serial.println("#100ms");
        ArduinoOTA.handle();
        mqttClient.loop();
        // wifiConnected = true;
        // timeoutEncoderAvailable = millis();
        checkwifiDisCon = true;
        wifidisConCnt = 0;
    }
    else
    {
        // Serial.println("#AA");
        flagWifiConnected = false;
        flagMQTTConnected = false;
        if (checkwifiDisCon)
        {
            Serial1.print(F("##wifi status : "));
            Serial1.println(WiFi.status());
            Serial1.print(F("##MQTTclintConnected :"));
            Serial1.println(mqttClient.connected());
            checkwifiDisCon = false;
        }
        checkWifiCon();
    }
}

void appTask100ms()
{
    //Serial.println("#100ms");
    // mqttClient.publish("/debug", String(alarmByte[0],HEX));
    if (flagWifiConnected && flagMQTTConnected)
    {
        digitalWrite(_PIN_LED_STS, !(digitalRead(_PIN_LED_STS)));
    }
    else
    {
        digitalWrite(_PIN_LED_STS, HIGH);
    }
}

void appTask500ms()
{
    //Serial.println("#500ms");
}

void appTask1s()
{
    //Serial.println("#1s");
}

void appTask4s()
{
}

void getModeSelect()
{
    byte _mode_A, _mode_B;
    ////// Machine A
    _mode_A = modbus_data[_BYTE_MODE_SELECTED_A];
    if ((_mode_A > _MODE_HIGH_MIN) && (_mode_A <= _MODE_HIGH_MAX))
        ModeSelect_A = 3; // Mode HIGH
    else if ((_mode_A > _MODE_MID_MIN) && (_mode_A <= _MODE_MID_MAX))
        ModeSelect_A = 2; // Mode MID
    else if ((_mode_A > _MODE_LOW_MIN) && (_mode_A <= _MODE_LOW_MAX))
        ModeSelect_A = 1; // Mode LOW
    else
        ModeSelect_A = 0;
    Serial1.println("Mode A : " + String(ModeSelect_A));
    ////// Machine B
    _mode_B = modbus_data[_BYTE_MODE_SELECTED_B];
    if ((_mode_B > _MODE_HIGH_MIN) && (_mode_B <= _MODE_HIGH_MAX))
        ModeSelect_B = 3; // Mode HIGH
    else if ((_mode_B > _MODE_MID_MIN) && (_mode_B <= _MODE_MID_MAX))
        ModeSelect_B = 2; // Mode MID
    else if ((_mode_B > _MODE_LOW_MIN) && (_mode_B <= _MODE_LOW_MAX))
        ModeSelect_B = 1; // Mode LOW
    else
        ModeSelect_B = 0;
    Serial1.println("Mode B : " + String(ModeSelect_B));
}

void getMoneyReceive()
{
    double _priceBuff_A = 0, _priceBuff_B = 0;
    uint16_t _dryTime_A, _dryTime_B;

    uint16_t time_MM_A = modbus_data[_BYTE_TIME_MM_A];
    uint16_t time_SS_A = modbus_data[_BYTE_TIME_SS_A];
    uint16_t time_MM_B = modbus_data[_BYTE_TIME_MM_B];
    uint16_t time_SS_B = modbus_data[_BYTE_TIME_SS_B];

    _dryTime_A = (time_MM_A * 60) + time_SS_A;
    _dryTime_B = (time_MM_B * 60) + time_SS_B;

    if (MachineDryLastStatus_A)
    {
        lastDryTime_MM_A = time_MM_A;
    }
    if (MachineDryLastStatus_B)
    {
        lastDryTime_MM_B = time_MM_B;
    }
    // Serial1.println("_dryTime_A : " + String(_dryTime_A));
    // Serial1.println("lastDryTime_A : " + String(lastDryTime_A));
    // Serial1.println("_dryTime_A : " + String(_dryTime_A));
    ///Machine A
    if (_dryTime_A > 300)
    {
        
        // _priceBuff_A = float((time_MM_A - lastDryTime_MM_A)) / float(_MINUTE_PER_COIN);
        // MoneyReceive_A += round(_priceBuff_A) * uint8_t(10);

        if (MoneyReceive_A > 0)
        {

            flag_CompareTime_A = true;
            if (mqttClient.connected())
                mqttClient.publish(MQTT_PubTopicDebug, "" + String(MachineID1) + " +Coin " + String(MoneyReceive_A));
        }
    }

    lastDryTime_MM_A = time_MM_A;
    lastDryTime_SS_A = time_SS_A;
    lastDryTime_A = _dryTime_A;

    ///Machine B
    //if ((_dryTime_B > lastDryTime_B) && (_dryTime_B > 300))
    if (_dryTime_B > 300)
    {
        // _priceBuff_B = float((time_MM_B - lastDryTime_MM_B)) / float(_MINUTE_PER_COIN);
        // MoneyReceive_B += round(_priceBuff_B) * uint8_t(10);
        if (MoneyReceive_B > 0)
        {
            flag_CompareTime_B = true;
            if (mqttClient.connected())
                mqttClient.publish(MQTT_PubTopicDebug, "" + String(MachineID2) + " +Coin " + String(MoneyReceive_B));
        }
    }

    lastDryTime_MM_B = time_MM_B;
    lastDryTime_SS_B = time_SS_B;
    lastDryTime_B = _dryTime_B;

    ////////////////////////////////////////
    //   if ((DryTime_B > LastDryTime_B) && (DryTime_B > 300))
    //   {
    //     _priceBuff_B = float((DryTime_MM_B - LastDryTime_MM_B)) / float(_MINUTE_PER_COIN);
    //     PriceIncome_B += round(_priceBuff_B) * uint8_t(10);
    //     if (PriceIncome_B > 0)
    //     {
    //       mqttClient.publish(MQTT_PubTopicDebug, "" + String(MachineID + 1) + " +Coin " + String(PriceIncome_B));
    //       mqttClient.publish(MQTT_PubTopicDebug, "" + String(MachineID + 1) + " Mode : " + ModeSelected_B);
    //       flag_CompareTime_B = true;
    //     }
    //   }
    //   LastDryTime_MM_B = DryTime_MM_B;
    //   LastDryTime_SS_B = DryTime_SS_B;
    //   LastDryTime_B = DryTime_B;
}
void getTotalMoney()
{
    TotalMoney_A = (modbus_data[_BYTE_TOTAL_MONEY_MSB_A] << 8 | modbus_data[_BYTE_TOTAL_MONEY_LSB_A]) * uint16_t(10);
    TotalMoney_B = (modbus_data[_BYTE_TOTAL_MONEY_MSB_B] << 8 | modbus_data[_BYTE_TOTAL_MONEY_LSB_B]) * uint16_t(10);
    // Serial1.print("TotalMoney_A : ");
    // Serial1.println(TotalMoney_A);
    // Serial1.print("TotalMoney_B : ");
    // Serial1.println(TotalMoney_B);
    if (TotalLastMoney_A == 0)
        TotalLastMoney_A = TotalMoney_A;

    if (TotalLastMoney_B == 0)
        TotalLastMoney_B = TotalMoney_B;

    // Serial1.println("Machine A Total Money : " + String(TotalMoney_A));
    // Serial1.println("TotalLastMoneyA:" + String(TotalLastMoney_A));
    if (TotalMoney_A != TotalLastMoney_A && TotalLastMoney_A != 0)
    {
        Serial1.print(F("Machine A Total Money : "));
        Serial1.println(TotalMoney_A);

        MoneyReceive_A += (TotalMoney_A - TotalLastMoney_A);
        Serial1.print(F("Machine A Insert Coin : " ));
        Serial1.println(MoneyReceive_A);

        ModePrice_A += (TotalMoney_A - TotalLastMoney_A);

        if (TotalMoney_A == 0)
        {
            sendStatementReport_A();
        }
        TotalLastMoney_A = TotalMoney_A;
    }

    if (TotalMoney_B != TotalLastMoney_B && TotalLastMoney_B != 0)
    {
        Serial1.print(F("Machine B Total Money : ")); 
        Serial1.println(TotalMoney_B);
        MoneyReceive_B += (TotalMoney_B - TotalLastMoney_B);
        Serial1.print(F("Machine B Insert Coin : "));
        Serial1.println(MoneyReceive_B);

        ModePrice_B += (TotalMoney_B - TotalLastMoney_B);

        if (TotalMoney_B == 0)
        {
            sendStatementReport_B();
        }

        TotalLastMoney_B = TotalMoney_B;
    }
}
void getMachineStatus()
{
    //Machine A
    switch (modbus_data[_BYTE_MACHINE_STATUS_A])
    {
    case 0x86: //ready to used
        Machine_A_Normal = true;
        Machine_A_Warning = false;
        Machine_A_Error = false;
        Machine_A_SettingMode = false;
        Machine_A_inUse = false;
        break;
    case 0x82: //ready after error
        Machine_A_Normal = false;
        Machine_A_Warning = true;
        Machine_A_Error = false;
        Machine_A_SettingMode = false;
        Machine_A_inUse = false;
        break;
    case 0xE2: //error
        Machine_A_Normal = false;
        Machine_A_Warning = false;
        Machine_A_Error = true;
        Machine_A_SettingMode = false;
        Machine_A_inUse = false;
        break;
    case 0xC2: //error acknowledge
        Machine_A_Normal = false;
        Machine_A_Warning = false;
        Machine_A_Error = true;
        Machine_A_SettingMode = false;
        Machine_A_inUse = false;
        break;
    case 0x81: //setting mode
        Machine_A_Normal = false;
        Machine_A_Warning = false;
        Machine_A_Error = false;
        Machine_A_SettingMode = true;
        Machine_A_inUse = false;
        break;
    case 0x14: //Drying
        Machine_A_Normal = false;
        Machine_A_Warning = false;
        Machine_A_Error = false;
        Machine_A_SettingMode = false;
        Machine_A_inUse = true;
        break;
    default:
        Machine_A_Normal = false;
        Machine_A_Warning = false;
        Machine_A_Error = false;
        Machine_A_SettingMode = false;
        Machine_A_inUse = false;
        break;
    }

    if ((Machine_A_Error != lastStatusError_A))
    {
        if (!getAfterError_A() && flag_DryingEven_A)
        {
            writeECodetoEEPROM_A(1);
            Serial1.println(F("machine A set e-code ERROR."));
        }
    }
    lastStatusError_A = Machine_A_Error;

    //Machine B
    switch (modbus_data[_BYTE_MACHINE_STATUS_B])
    {
    case 0x86: //ready to used
        Machine_B_Normal = true;
        Machine_B_Warning = false;
        Machine_B_Error = false;
        Machine_B_SettingMode = false;
        Machine_B_inUse = false;
        break;
    case 0x82: //ready after error
        Machine_B_Normal = false;
        Machine_B_Warning = true;
        Machine_B_Error = false;
        Machine_B_SettingMode = false;
        Machine_B_inUse = false;
        break;
    case 0xE2: //error
        Machine_B_Normal = false;
        Machine_B_Warning = false;
        Machine_B_Error = true;
        Machine_B_SettingMode = false;
        Machine_B_inUse = false;
        break;
    case 0xC2: //error acknowledge
        Machine_B_Normal = false;
        Machine_B_Warning = false;
        Machine_B_Error = true;
        Machine_B_SettingMode = false;
        Machine_B_inUse = false;
        break;
    case 0x81: //setting mode
        Machine_B_Normal = false;
        Machine_B_Warning = false;
        Machine_B_Error = false;
        Machine_B_SettingMode = true;
        Machine_B_inUse = false;
        break;
    case 0x14: //Drying
        Machine_B_Normal = false;
        Machine_B_Warning = false;
        Machine_B_Error = false;
        Machine_B_SettingMode = false;
        Machine_B_inUse = true;
        break;
    default:
        Machine_B_Normal = false;
        Machine_B_Warning = false;
        Machine_B_Error = false;
        Machine_B_SettingMode = false;
        Machine_B_inUse = false;
        break;
    }

    if ((Machine_B_Error != lastStatusError_B))
    {
        if (!getAfterError_B() && flag_DryingEven_B)
        {
            writeECodetoEEPROM_B(1);
            Serial1.println(F("machine B set e-code ERROR."));
        }
    }
    lastStatusError_B = Machine_B_Error;
}

void checkDryEven_A()
{

    if (Machine_A_inUse && !MachineDryLastStatus_A)
    {

        if ((!flag_DryingEven_A || flag_CompareTime_A) && (MoneyReceive_A > 0))
        {

            //debug on mqtt
            if (mqttClient.connected())
                mqttClient.publish(MQTT_PubTopicDebug, "" + String(MachineID1) + " dry A start.");

            // if(getAfterError()) MoneyReceive = 0;
            statusReady_A = false;
            machineRunning_A = true;
            flag_DryingEven_A = true;
            flag_CompareTime_A = false;
            // Serial1.println(ModeSelect);
            // Serial1.println(ModePrice);
            // Serial1.println(MoneyReceive);
            // Serial1.println(modbus_data[_BYTE_TIME_MM]);
            // Serial1.println(modbus_data[_BYTE_TIME_SS]);

            SendTransectionReport_A();

            //write flag machine drying
            MachineDryLastStatus_A = 1;
            EEPROM.write(_ADDRESS_MACHINE_DRYING_A, MachineDryLastStatus_A);
            MachineDryLastStatus_A = 0;
            EEPROM.commit();
        }
    }

    if (Machine_A_Normal && (!Machine_A_inUse))
    {
        if (flag_DryingEven_A)
        {
            machineRunning_A = false;
            statusReady_A = true;
            ModePrice_A = 0;
            MachineDryLastStatus_A = 0;
            Serial1.println(F("### dry A complete. ###"));
            flag_DryingEven_A = false;

            //reset flag machine drying
            EEPROM.write(_ADDRESS_MACHINE_DRYING_A, MachineDryLastStatus_A);
            EEPROM.commit();

            //debug on mqtt
            if (mqttClient.connected())
                mqttClient.publish(MQTT_PubTopicDebug, "" + String(MachineID1) + " dry A complete.");
        }
    }
}

void checkDryEven_B()
{
    if (Machine_B_inUse && !MachineDryLastStatus_B)
    {
        if ((!flag_DryingEven_B || flag_CompareTime_B) && (MoneyReceive_B > 0))
        {
            //debug on mqtt
            if (mqttClient.connected())
                mqttClient.publish(MQTT_PubTopicDebug, "" + String(MachineID2) + " dry B start.");

            // if(getAfterError()) MoneyReceive = 0;
            statusReady_B = false;
            machineRunning_B = true;
            flag_DryingEven_B = true;

            flag_CompareTime_B = false;
            // Serial1.println(ModeSelect);
            // Serial1.println(ModePrice);
            // Serial1.println(MoneyReceive);
            // Serial1.println(modbus_data[_BYTE_TIME_MM]);
            // Serial1.println(modbus_data[_BYTE_TIME_SS]);

            SendTransectionReport_B();

            //write flag machine drying
            MachineDryLastStatus_B = 1;
            EEPROM.write(_ADDRESS_MACHINE_DRYING_B, MachineDryLastStatus_B);
            EEPROM.commit();
            MachineDryLastStatus_B = 0;
        }
    }

    if (Machine_B_Normal && (!Machine_B_inUse))
    {
        if (flag_DryingEven_B)
        {
            machineRunning_B = false;
            statusReady_B = true;
            ModePrice_B = 0;
            MachineDryLastStatus_B = 0;
            Serial1.println(F("### dry B complete. ###"));
            flag_DryingEven_B = false;

            //reset flag machine drying
            EEPROM.write(_ADDRESS_MACHINE_DRYING_B, MachineDryLastStatus_B);
            EEPROM.commit();

            //debug on mqtt
            if (mqttClient.connected())
                mqttClient.publish(MQTT_PubTopicDebug, "" + String(MachineID2) + " dry B complete.");
        }
    }
}

void getErrorCode()
{
    // ErrorCode[0] = modbus_data[_BYTE_ALARM_REPORT_LSB_1];
    // ErrorCode[1] = modbus_data[_BYTE_ALARM_REPORT_MSB_1];
    // ErrorCode[2] = modbus_data[_BYTE_ALARM_REPORT_LSB_2];
    // ErrorCode[3] = modbus_data[_BYTE_ALARM_REPORT_MSB_2];
    // ErrorCode[4] = modbus_data[_BYTE_ALARM_REPORT_LSB_3];
    // ErrorCode[5] = modbus_data[_BYTE_ALARM_REPORT_MSB_3];
    ErrorCode[0] = modbus_data[_BYTE_ALARM_REPORT_A];
    ErrorCode[1] = modbus_data[_BYTE_ALARM_REPORT_B];
    ErrorCodeCheck();
}

void ErrorCodeCheck()
{
    // uint8_t loop = 0;
    // for (uint8_t i = 0; i < _ERROR_CODE_BYTE_CNT; i++)
    // {
    //     for (uint8_t j = 0; j < 8; j++)
    //     {
    //         loop = (i + j + (i * 7));
    //         ErrorCodeBit[loop] = bitRead(ErrorCode[i], j);
    //     }
    // }
    uint8_t loop = 0;
    for (uint8_t j = 0; j < 8; j++)
    {
        loop = j;
        ErrorCodeBit_A[loop] = bitRead(ErrorCode[0], j);
        ErrorCodeBit_B[loop] = bitRead(ErrorCode[1], j);

        lastErrorCodeBit_A[loop] = ErrorCodeBit_A[loop];
        lastErrorCodeBit_B[loop] = ErrorCodeBit_B[loop];
    }
}

void checkSettingMode()
{
    // if (Machine_A_SettingMode || Machine_B_SettingMode)
    // {
    //     // Serial1.println("Setting mode.");
    //     if ((!ModePrice_A && !ModeSelect_A) || (!ModePrice_B && !ModeSelect_B))
    //     {
    //         // Serial1.println("Setting mode. = 0");
    //         if (TotalMoney_A != lastTotalMoney_A)
    //         {
    //             // Serial1.println("Setting mode total");
    //             if (lastTotalMoney_A > TotalMoney_A)
    //             {
    //                 //clear data
    //                 // sendStatementReport();
    //                 Serial1.println("Clear total Money dry A.");
    //             }
    //         }
    //         lastTotalMoney_A = TotalMoney_A;

    //         // Serial1.println("Setting mode. = 0");
    //         if (TotalMoney_B != lastTotalMoney_B)
    //         {
    //             // Serial1.println("Setting mode total");
    //             if (lastTotalMoney_B > TotalMoney_B)
    //             {
    //                 //clear data
    //                 // sendStatementReport();
    //                 Serial1.println("Clear total Money dry B.");
    //             }
    //         }
    //         lastTotalMoney_B = TotalMoney_B;
    //     }
    // }
    ////Machie A
    // if (TotalMoney_A != lastTotalMoney_A)
    // {
    //     if (TotalMoney_A < lastTotalMoney_A)
    //     {
    //         sendStatementReport_A();
    //         Serial1.println("Clear total Money A.");
    //     }

    //     lastTotalMoney_A = TotalMoney_A;
    // }
    // ////Machie B
    // if (TotalMoney_B != lastTotalMoney_B)
    // {
    //     if (TotalMoney_B < lastTotalMoney_B)
    //     {
    //         sendStatementReport_B();
    //         Serial1.println("Clear total Money B.");
    //     }

    //     lastTotalMoney_B = TotalMoney_B;
    // }
}

uint8_t getMachineID()
{
    int _m_id = EEPROM.read(_ADDRESS_MACHINE_ID);
    const char *_clientName;

    Serial1.print(F("Machine ID = "));
    Serial1.println(_m_id);
    MachineID1 = _m_id;
    MachineID2 = MachineID1 + 1;
    MQTT_ClientName += String(_m_id, DEC);
    Serial1.print(F("Client Name = "));
    Serial1.println(MQTT_ClientName);

    if (MachineID1 == 0 || MachineID1 > 200)
        setMachine(99);
}

void setMachine(uint8_t _id)
{
    EEPROM.write(_ADDRESS_MACHINE_ID, _id);
    EEPROM.commit();
    int i = 3;
    while (i > 0)
    {
        Serial1.print(F("Restart in "));
        Serial1.println(i);
        i--;
        delay(1000);
    }
    ESP.reset();
}

void messageReceived(String &topic, String &payload)
{
    StaticJsonDocument<200> doc;

    DeserializationError error = deserializeJson(doc, payload);

    // Test if parsing succeeds.
    if (error)
    {
        Serial1.print(F("deserializeJson() failed: "));
        Serial1.println(error.c_str());
        return;
    }

    String msg = doc[F("message")];
    int _id = doc[F("id")];
    int _sn = doc[F("sn")];
    int _cid = doc[F("cid")];
    int _nid = doc[F("nid")];
    //debugSerial.println("#1 "+ payload);
    if ((topic.indexOf(String(MQTT_SubTopicReturn)) != -1))
    {
        if ((_id == MachineID1) || (_sn == MachineID1))
        {
            ReturnValue = msg;
            checkReturnSuccess();
            Serial1.print(F("A return = "));
            Serial1.println(msg);
        }

        if ((_id == MachineID2) || (_sn == MachineID2))
        {
            ReturnValue = msg;
            checkReturnSuccess();
            Serial1.print(F("B return = "));
            Serial1.println(msg);
        }
    }

    else if ((topic.indexOf(String(MQTT_SubTopicCHID)) != -1))
    {
        if (_cid != MachineID1)
        {
            mqttClient.publish(MQTT_PubTopicDebug, "current id incorrect.");
        }

        else if (_cid == MachineID1 && _nid != MachineID1 && _nid > 0)
        {
            //set New ID
            MachineID1 = _nid;
            if (mqttClient.connected())
            {
                mqttClient.publish(MQTT_PubTopicDebug, "Dry machine change id from " + String(_cid) + " to " + String(MachineID1));
                mqttClient.publish(MQTT_PubTopicDebug, "Dry machine change id from " + String(_cid + 1) + " to " + String(MachineID1 + 1));
            }
            setMachine(MachineID1);
        }
    }
}

void checkResetESP()
{
    if (wifidisConCnt >= _LOOP_TRY_TO_CONNECT_WIFI)
    {
        if (!flag_DryingEven_A && !flag_DryingEven_B)
        {
            Serial1.println(F("ESP RESET"));
            ESP.reset();
        }
    }
}

void checkWifiCon()
{
    if (millis() - loopTimeConnectWifi > _TIME_CONNECT_WIFI)
    {
        wifidisConCnt++;
        WiFi.disconnect(true);
        Serial1.println(F("Connecting to Wifi."));
#ifdef _WIFI_LOCAL
        WiFi.config(ip, gateway, subnet);
#endif
        WiFi.mode(WIFI_STA);
        WiFi.begin(WiFissid, WiFipass);
        ConnectWiFi();
        loopTimeConnectWifi = millis();
    }
}

void loopSendStatus(uint32_t _time)
{
    if ((millis() - timeSendStatus > _time) || (millis()) < timeSendStatus)
    {
        SendStatustoCloudBox_A();
        SendStatustoCloudBox_B();
        timeSendStatus = millis();
    }
}

void SendStatustoCloudBox_A()
{
    StaticJsonDocument<200> jsonUpdateGen;
    //JsonObject &jsonUpdateGen = jsonUpdateBuffer.createObject();
    String _jsonOut = F(" ");
    String _buff = F(" ");
    String _alOut = F(" ");

    jsonUpdateGen[F("id")] = String(MachineID1);
    //debugSerial.println(flag_Error_Status);
    if (Machine_A_Error)
    {
        for (int i = 0; i < (_ERROR_CODE_BYTE_CNT * 8); i++)
        {
            if (ErrorCodeBit_A[i] == true)
            {
                _buff += String(i + 1) + ",";
            }
        }

        _alOut = _buff.substring(0, (_buff.length() - 1));
        jsonUpdateGen[F("status")] = "ERROR";
        jsonUpdateGen[F("e-code")] = _alOut;
        serializeJson(jsonUpdateGen, _jsonOut);
        if (mqttClient.connected())
        {
            mqttClient.publish(MQTT_PubTopicStatus, _jsonOut);
            Serial1.println(F("error status A."));
        }
    }

    else
    {
        jsonUpdateGen[F("status")] = "READY";
        jsonUpdateGen[F("wifi")] = WiFi.RSSI();
        // jsonUpdateGen[F("stage")] = String(MachineDryLastStatus_A);
        //jsonUpdateGen[F("e-code")] = "-";
        serializeJson(jsonUpdateGen, _jsonOut);
        if (mqttClient.connected())
        {
            mqttClient.publish(MQTT_PubTopicStatus, _jsonOut);
            //Serial1.println(F("##test ready status A."));
            if(machineRunning_A == false)
            statusReady_A = true;
            
            _jsonOut = " ";
        }
    }
}

void SendStatustoCloudBox_B()
{
    // StaticJsonDocument<200> jsonUpdateBuffer;
    StaticJsonDocument<200> jsonUpdateGen;

    String _jsonOut = F(" ");
    String _buff = F(" ");
    String _alOut = F(" ");

    jsonUpdateGen[F("id")] = String(MachineID2);
    //debugSerial.println(flag_Error_Status);
    if (Machine_B_Error)
    {
        for (int i = 0; i < (_ERROR_CODE_BYTE_CNT * 8); i++)
        {
            if (ErrorCodeBit_B[i] == true)
            {
                _buff += String(i + 1) + ",";
            }
        }

        _alOut = _buff.substring(0, (_buff.length() - 1));
        jsonUpdateGen[F("status")] = "ERROR";
        jsonUpdateGen[F("e-code")] = _alOut;
        serializeJson(jsonUpdateGen, _jsonOut);
        if (mqttClient.connected())
        {
            mqttClient.publish(MQTT_PubTopicStatus, _jsonOut);
            Serial1.println(F("error status B."));
        }
    }

    else
    {
        jsonUpdateGen[F("status")] = "READY";
        jsonUpdateGen[F("wifi")] = WiFi.RSSI();
        // jsonUpdateGen[F("stage")] = String(MachineDryLastStatus_B);
        //jsonUpdateGen[F("e-code")] = "-";
        serializeJson(jsonUpdateGen, _jsonOut);
        if (mqttClient.connected())
        {
            mqttClient.publish(MQTT_PubTopicStatus, _jsonOut);
            //Serial1.println(F("ready status B."));
            if(machineRunning_B == false)
            statusReady_B = true;

            _jsonOut = " ";
        }
    }
}

void SendTransectionReport_A()
{
    StaticJsonDocument<400> doc;
    // StaticJsonDocument<400> jsonBuffer;
    // JsonObject &jsonGen = jsonBuffer.createObject();
    // JsonObject &jsonValue = jsonBuffer.createObject();

    String _jsonPrint = F(" ");
    TransactionReport[0] = String(MachineID1);

    if (ModeSelect_A == 1)
        TransactionReport[1] = "LOW";
    else if (ModeSelect_A == 2)
        TransactionReport[1] = "MID";
    else if (ModeSelect_A == 3)
        TransactionReport[1] = "HIGH";
    else
        TransactionReport[1] = "";
    //Get mode price
    Serial1.println("###loop_SendTransectionReport_A");

    TransactionReport[2] = String(ModePrice_A, DEC);
    TransactionReport[3] = String(MoneyReceive_A, DEC);
    TransactionReport[4] = (String(lastDryTime_MM_A, DEC) + " : " + String(lastDryTime_SS_A, DEC));

    if (TransactionReport[1] != "" && TransactionReport[2] != "" && TransactionReport[3] != "" && TransactionReport[4] != "0 : 0")
    {
        doc[F("table")] = "2";
        doc[F("mid")] = TransBuffWait;

        JsonObject api = doc.createNestedObject("api");
        api[F("1")] = TransactionReport[0];
        api[F("2")] = TransactionReport[1];
        api[F("3")] = TransactionReport[2];
        api[F("4")] = TransactionReport[3];
        api[F("5")] = TransactionReport[4];

        // jsonValue[F("api")] = jsonGen;
        //jsonBuffer.Clear();
        Transbuff[TransBuffWait] = F(" ");
        serializeJson(doc, Transbuff[TransBuffWait]);
        // Serial1.print(F("wait = "));
        // Serial1.println(TransBuffWait);
        Serial1.println(F("###SendTransectionReport_A###"));
        Serial1.println(Transbuff[TransBuffWait]);
        StoreBufferCount();
        TransactionReport[0] = F(" ");
        TransactionReport[1] = F(" ");
        TransactionReport[2] = F(" ");
        TransactionReport[3] = F(" ");
        TransactionReport[4] = F(" ");
        MoneyReceive_A = 0;
    }
}

void SendTransectionReport_B()
{
    StaticJsonDocument<400> doc;
    // JsonObject &jsonGen = jsonBuffer.createObject();
    // JsonObject &jsonValue = jsonBuffer.createObject();

    String _jsonPrint = F(" ");
    TransactionReport[0] = String(MachineID2);

    if (ModeSelect_B == 1)
        TransactionReport[1] = "LOW";
    else if (ModeSelect_B == 2)
        TransactionReport[1] = "MID";
    else if (ModeSelect_B == 3)
        TransactionReport[1] = "HIGH";
    else
        TransactionReport[1] = "";

    //Get mode price
    // ModePrice_B += MoneyReceive_B;

    TransactionReport[2] = String(ModePrice_B, DEC);
    TransactionReport[3] = String(MoneyReceive_B, DEC);
    TransactionReport[4] = (String(lastDryTime_MM_B, DEC) + " : " + String(lastDryTime_SS_B, DEC));

    if (TransactionReport[1] != "" && TransactionReport[2] != "" && TransactionReport[3] != "" && TransactionReport[4] != "0 : 0")
    {
        // jsonGen[F("1")] = TransactionReport[0];
        // jsonGen[F("2")] = TransactionReport[1];
        // jsonGen[F("3")] = TransactionReport[2];
        // jsonGen[F("4")] = TransactionReport[3];
        // jsonGen[F("5")] = TransactionReport[4];
        // jsonValue[F("table")] = "2";
        // jsonValue[F("mid")] = TransBuffWait;
        // jsonValue[F("api")] = jsonGen;

        doc[F("table")] = "2";
        doc[F("mid")] = TransBuffWait;
        JsonObject api = doc.createNestedObject("api");
        api[F("1")] = TransactionReport[0];
        api[F("2")] = TransactionReport[1];
        api[F("3")] = TransactionReport[2];
        api[F("4")] = TransactionReport[3];
        api[F("5")] = TransactionReport[4];
        //jsonBuffer.Clear();
        Transbuff[TransBuffWait] = F(" ");
        serializeJson(doc, Transbuff[TransBuffWait]);
        // Serial1.print(F("wait = "));
        // Serial1.println(TransBuffWait);
        Serial1.println(F("###SendTransectionReport_B###"));
        Serial1.println(Transbuff[TransBuffWait]);
        StoreBufferCount();
        TransactionReport[0] = F(" ");
        TransactionReport[1] = F(" ");
        TransactionReport[2] = F(" ");
        TransactionReport[3] = F(" ");
        TransactionReport[4] = F(" ");
        MoneyReceive_B = 0;
    }
}

uint16_t apiDataToSendAvailable(void)
{
    return (uint16_t)(_TRANS_BUFF_LEN + TransBuffWait - TransBuffSend) % _TRANS_BUFF_LEN;
}

void LoopCheckRemainBuff()
{
    if (apiDataToSendAvailable())
    {
        if (mqttClient.connected())
        {
            loopSendTransCnt++;
            mqttClient.publish(MQTT_PubTopicReport, Transbuff[TransBuffSend]);
        }

        if (loopSendTransCnt >= _LOOPTIME_SEND)
        {
            if (mqttClient.connected())
            {
                mqttClient.publish(MQTT_PubTopicDebug, "Send transection timeout.");
            }
            TransBuffSend = uint8_t(TransBuffSend + 1) % _TRANS_BUFF_LEN;
            loopSendTransCnt = 0;
        }
    }
}

void checkReturnSuccess()
{
    if (ReturnValue == F("success"))
    {
        TransBuffSend = uint8_t(TransBuffSend + 1) % _TRANS_BUFF_LEN;
        ReturnValue = F(" ");
        LoopCheckRemainBuff();
        loopSendTransCnt = 0;
    }
}

void StoreBufferCount()
{
    TransBuffWait = uint8_t(TransBuffWait + 1) % _TRANS_BUFF_LEN;
}

bool getAfterError_A()
{
    if (!EEPROM.read(_ADDRESS_ERROR_CODE_A))
        return 0;
    else
        return 1;
}

void writeECodetoEEPROM_A(bool _value)
{
    EEPROM.write(_ADDRESS_ERROR_CODE_A, _value);
    EEPROM.commit();
}

bool getAfterError_B()
{
    if (!EEPROM.read(_ADDRESS_ERROR_CODE_B))
        return 0;
    else
        return 1;
}

void writeECodetoEEPROM_B(bool _value)
{
    EEPROM.write(_ADDRESS_ERROR_CODE_B, _value);
    EEPROM.commit();
}

void sendStatementReport_A()
{
    String _jsonOut = " ";
    String _buff;
    StaticJsonDocument<200> _jsonUpdateBuffer;
    // JsonObject &_jsonUpdateGen1 = _jsonUpdateBuffer.createObject();
    // JsonObject &_jsonUpdateGen2 = _jsonUpdateBuffer.createObject();

    // _jsonUpdateGen2[F("1")] = String(MachineID1);
    // _jsonUpdateGen2[F("2")] = String(lastTotalMoney_A);

    // _jsonUpdateGen1[F("table")] = "3";
    // _jsonUpdateGen1[F("mid")] = TransBuffWait;
    // _jsonUpdateGen1[F("api")] = _jsonUpdateGen2;
    // Transbuff[TransBuffWait] = F(" ");
    // _jsonUpdateGen1.printTo(Transbuff[TransBuffWait]);

    _jsonUpdateBuffer[F("table")] = "3";
    _jsonUpdateBuffer[F("mid")] = TransBuffWait;
    JsonObject api = _jsonUpdateBuffer.createNestedObject("api");
    api[F("1")] = String(MachineID1);
    api[F("2")] = String(lastTotalMoney_A);
    Transbuff[TransBuffWait] = F(" ");
    serializeJson(_jsonUpdateBuffer, Transbuff[TransBuffWait]);
    StoreBufferCount();
    if (mqttClient.connected())
        mqttClient.publish(MQTT_PubTopicDebug, "" + String(MachineID1) + " Clear money A.");

    _jsonOut = F(" ");
}

void sendStatementReport_B()
{
    String _jsonOut = " ";
    String _buff;
    StaticJsonDocument<200> _jsonUpdateBuffer;
    // JsonObject &_jsonUpdateGen1 = _jsonUpdateBuffer.createObject();
    // JsonObject &_jsonUpdateGen2 = _jsonUpdateBuffer.createObject();

    _jsonUpdateBuffer[F("1")] = String(MachineID2);
    _jsonUpdateBuffer[F("2")] = String(lastTotalMoney_B);

    _jsonUpdateBuffer[F("table")] = "3";
    _jsonUpdateBuffer[F("mid")] = TransBuffWait;
    _jsonUpdateBuffer[F("api")] = _jsonUpdateBuffer;
    Transbuff[TransBuffWait] = F(" ");
    serializeJson(_jsonUpdateBuffer, Transbuff[TransBuffWait]);
    // _jsonUpdateGen1.printTo(Transbuff[TransBuffWait]);
    StoreBufferCount();
    if (mqttClient.connected())
        mqttClient.publish(MQTT_PubTopicDebug, "" + String(MachineID2) + " Clear money B.");

    _jsonOut = F(" ");
}

///// modbus
bool modbus_setSlaveId(byte slaveId)
{
    _slaveId = slaveId;
    return true;
}

byte modbus_getSlaveId()
{
    return _slaveId;
}

void modbus_task()
{
    // if(Serial.available())
    // {
    //     // Serial1.print(Serial.read(),HEX);
    //     Serial.read();
    //     Serial1.println(millis() - timeMBread);
    // }
    // timeMBread = millis();
    _len = 0;
    while (Serial.available() > _len)
    {
        _len = Serial.available();
        delayMicroseconds(15000000 / 9600);
    }

    if (_len == 0)
        return;

    // Serial1.println(_len);

    byte i;
    // if (_len < 10)
    // {
    //     for (int x = 0; x < _len; x++)
    //         Serial.read();
    //     return;
    // }
    // _frame = (byte *)malloc(_DATA_SIZE);
    if (_len == _DATA_SIZE)
    {
        // Serial1.println(_len);
        for (i = 0; i < _len; i++)
        {
            modbus_data[i] = Serial.read();
            // Serial1.print(i);
            // Serial1.print(" ");
            // Serial1.print(modbus_data[i],HEX);
            // Serial1.print(" ");
        }
        // _len2 = _len;
        // Serial1.println();
        // return;
    }
    else
    {
        /* code */
        for (int x = 0; x < _len; x++)
            Serial.read();
        return;
    }

    // Serial1.println(_len);
    // for (i = (_len2); i < (_len2 + _len); i++)
    // {
    // modbus_data[i] = Serial.read();
    // Serial1.print(modbus_data[i],HEX);
    // Serial1.print(" ");
    // Serial1.print(i);
    // Serial1.print(" ");
    // Serial1.print(" ");
    // data[i+_len2] = _frame[i];
    // }
    if (modbus_receive(modbus_data) && (_len) == _DATA_SIZE)
        modbus_dataCorrect = true;

    _len = 0;
    _len2 = 0;
    // free(_frame);
}

bool modbus_receive(byte *frame)
{
    //first byte of frame = address
    byte address = frame[0];
    //Last two bytes = crc
    uint16_t crc = ((frame[(_len)-2] << 8) | frame[(_len)-1]);
    //Slave Check
    if (address != 0xFF && address != modbus_getSlaveId())
    {
        return false;
    }
    //CRC Check
    if (crc != modbus_calcCrc(frame[0], frame + 1, (_len)-3))
    {
        Serial1.println("CRC false");
        return false;
    }
    // Serial1.println("CRC true");
    // return true;
}

word modbus_calcCrc(byte address, byte *pduFrame, byte pduLen)
{
    byte CRCHi = 0xFF, CRCLo = 0x0FF, Index;

    Index = CRCHi ^ address;
    CRCHi = CRCLo ^ _auchCRCHi[Index];
    CRCLo = _auchCRCLo[Index];

    while (pduLen--)
    {
        Index = CRCHi ^ *pduFrame++;
        CRCHi = CRCLo ^ _auchCRCHi[Index];
        CRCLo = _auchCRCLo[Index];
    }

    return (CRCHi << 8) | CRCLo;
}

void modbus_clearData()
{
    for (int x = 0; x < _DATA_SIZE; x++)
        modbus_data[x] = 0;
} 

void loopStatusWork()
{
    if (machineRunning_A)
    {
        Serial1.println(F("machine Running A"));
    }
    else if (statusReady_A)
    {
        Serial1.println(F("ready status A."));
    }
    else
    {
    }

    if (machineRunning_B)
    {
        Serial1.println(F("machine Running B"));
    }
    else if (statusReady_B)
    {
        Serial1.println(F("ready status B."));
    }
    else
    {
    }
}