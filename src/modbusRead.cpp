#include "modbusRead.h"

void ModbusRead::begin(HardwareSerial *port)
{
    _port = port;
    _port->begin(9600);

    _t15 = 15000000 / 9600; // 1T * 1.5 = T1.5
    _t35 = 35000000 / 9600; // 1T * 3.5 = T3.5
}

void ModbusRead::begin(HardwareSerial *port, long baud)
{
    _port = port;
    _port->begin(baud);

    if (baud > 19200)
    {
        _t15 = 750;
        _t35 = 1750;
    }
    else
    {
        _t15 = 15000000 / baud; // 1T * 1.5 = T1.5
        _t35 = 35000000 / baud; // 1T * 3.5 = T3.5
    }
}

void ModbusRead::begin(HardwareSerial *port, long baud, SerialConfig parity)
{
    _port = port;
    _port->begin(baud, parity);

    if (baud > 19200)
    {
        _t15 = 750;
        _t35 = 1750;
    }
    else
    {
        _t15 = 15000000 / baud; // 1T * 1.5 = T1.5
        _t35 = 35000000 / baud; // 1T * 3.5 = T3.5
    }
    // Serial1.println("OK");
}

bool ModbusRead::setSlaveId(byte slaveId)
{
    _slaveId = slaveId;
    return true;
}

byte ModbusRead::getSlaveId()
{
    return _slaveId;
}

void ModbusRead::task()
{ 
    if(_port->available())
    {
        // Serial.print(_port->read(),HEX);
        // Serial.print("AA");
    }
    // _len = 0;
    // while ((*_port).available() > _len)
    // {
    //     _len = (*_port).available();
    //     delayMicroseconds(_t15);
    // }


    // if (_len == 0)
    //     return;

    // byte i;
    // if (_len < 10)
    // {
    //     for (int x = 0; x < _len; x++) (*_port).read();
    //     return;
    // }

    // Serial1.println(_len);

    // _frame = (byte *)malloc(189);
    // if(_len > 120)
    // {
    //     for (i = 0; i < _len; i++)
    //     {
    //         data[i] = (*_port).read();
            // Serial1.print(i);
            // Serial1.print(" ");
            // Serial1.print(_frame[i],HEX);
            // Serial1.print(" ");
            // data[i] = _frame[i];
    //     }
    //     _len2 = _len;
    //     return;
    // }
    // Serial1.println();
    // Serial1.println("OK");
    // Serial1.println(_len+_len2);
    // Serial1.println(_len2);
// for (int x = 0; x < _len; x++) _port->read();

    // _frame2 = (byte *)malloc(_len+_len2);
    // for (i = 0; i < (_len2); i++)
    // {
    //     _frame2[i] = _frame[i];
    //     Serial1.print(_frame[i],HEX);
    //     Serial1.print(" ");
    //     // data[i+_len2] = _frame[i];
    // }
    // free(_frame);
    // for (i = (_len2); i < (_len2+_len); i++)
    // {
        // data[i] = (*_port).read();
        // Serial1.print(i);
        // Serial1.print(" ");
    //     Serial1.print(" ");
    //     // data[i+_len2] = _frame[i];
    // }
    // Serial1.println("");
    // if (receive(data) && (_len+_len2) == _DATA_SIZE) dataCorrect = true;

    //  _len = 0;
    //  _len2 = 0;
    //  data[_DATA_SIZE] = {0};
    // free(_frame);
}

bool ModbusRead::receive(byte frame[_DATA_SIZE])
{
    //first byte of frame = address
    byte address = frame[0];
    //Last two bytes = crc
    uint16_t crc = ((frame[(_len+_len2) - 2] << 8) | frame[(_len+_len2) - 1]);
    //Slave Check
    if (address != 0xFF && address != this->getSlaveId())
    {
        return false;
    }
    //CRC Check
    if (crc != this->calcCrc(_frame[0], _frame + 1, (_len+_len2) - 3))
    {
        return false;
    }
    return true;
}

word ModbusRead::calcCrc(byte address, byte *pduFrame, byte pduLen)
{
    byte CRCHi = 0xFF, CRCLo = 0x0FF, Index;

    Index = CRCHi ^ address;
    CRCHi = CRCLo ^ _auchCRCHiA[Index];
    CRCLo = _auchCRCLoB[Index];

    while (pduLen--)
    {
        Index = CRCHi ^ *pduFrame++;
        CRCHi = CRCLo ^ _auchCRCHiA[Index];
        CRCLo = _auchCRCLoB[Index];
    }

    return (CRCHi << 8) | CRCLo;
}

void ModbusRead::clearData()
{
    for (int x = 0; x < _DATA_SIZE; x++)
        data[x] = 0;
}