// sm_timetask.h

#ifndef _SM_TIMETASK_h
#define _SM_TIMETASK_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

void appTask1ms();
void appTask10ms();
void appTask100ms();
void appTask500ms(); 
void appTask1s();
void appTask4s();

class SM_TIMETASK 
{
	private:
		unsigned char time_1ms_cnt;
		unsigned char time_10ms_cnt;
		unsigned char time_100ms_cnt;
		unsigned char time_500ms_cnt;
		unsigned char time_1s_cnt;
		unsigned char time_4s_cnt;

		uint32_t currentMillis = 0;
		uint32_t previousMillis = 0;
		uint32_t lastTime;
		void timer_task(void);

		bool time_1ms_flag;
		bool time_10ms_flag;
		bool time_100ms_flag;
		bool time_500ms_flag;
		bool time_1s_flag;
		bool time_4s_flag;
		void update_timer_cnt(void);
		bool flag_StartStop = false;
	public:	
		void Start(void);
		void Stop(void);
		void StartStop(bool);
};

#endif