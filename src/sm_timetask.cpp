#include "sm_timetask.h"

void SM_TIMETASK::timer_task(void)
{
	if (time_1ms_flag)
	{
		appTask1ms();
		time_1ms_flag = false;
	}
	if (time_10ms_flag)
	{
		appTask10ms();
		time_10ms_flag = false;
	}
	if (time_100ms_flag)
	{
		appTask100ms();
		time_100ms_flag = false;
	}
	if (time_500ms_flag)
	{
		appTask500ms();
		time_500ms_flag = false;
	}
	if (time_1s_flag)
	{
		appTask1s();
		time_1s_flag = false;
	}
	if (time_4s_flag)
	{
		appTask4s();
		time_4s_flag = false;
	}

	currentMillis = millis();
	if (currentMillis - previousMillis > 1)
		update_timer_cnt();
	else if (currentMillis < previousMillis)
		previousMillis = currentMillis;
}

void SM_TIMETASK::update_timer_cnt(void)
{
	previousMillis = currentMillis;
	time_1ms_cnt++;
	time_1ms_flag = true;
	if (time_1ms_cnt >= 10) {
		time_1ms_cnt = 0;
		time_10ms_cnt++;
		time_10ms_flag = true;
		if (time_10ms_cnt >= 10) {
			time_10ms_cnt = 0;
			time_100ms_cnt++;
			time_100ms_flag = true;
			if (time_100ms_cnt >= 5) {
				time_100ms_cnt = 0;
				time_500ms_cnt++;
				time_500ms_flag = true;
				if (time_500ms_cnt >= 2) {
					time_500ms_cnt = 0;
					time_1s_cnt++;
					time_1s_flag = true;
					if (time_1s_cnt >= 4) {
						time_1s_cnt = 0;
						time_4s_cnt++;
						time_4s_flag = true;
					}
				}
			}
		}
	}
}

void SM_TIMETASK::Start()
{
	flag_StartStop = true;
	StartStop(flag_StartStop);
}

void SM_TIMETASK::Stop()
{
	flag_StartStop = false;
	StartStop(flag_StartStop);
}

void SM_TIMETASK::StartStop(bool en)
{
	if (en)
		timer_task();
}
